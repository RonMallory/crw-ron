# crw-ron

This project is a sample project to proof of concept codeready workspace with a few sample apps, starting with nodejs.

## getting started

Using RH developer sandbox, your url may change.  Modify this url depending on what url for your cluster.

[https://codeready-codeready-workspaces-operator.apps.sandbox.x8i5.p1.openshiftapps.com/f?url=https://gitlab.com/RonMallory/crw-ron/-/raw/main/nodejs-devfile.yaml](https://codeready-codeready-workspaces-operator.apps.sandbox.x8i5.p1.openshiftapps.com/f?url=https://gitlab.com/RonMallory/crw-ron/-/raw/main/nodejs-devfile.yaml)
